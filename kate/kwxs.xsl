<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:kwix="http://www.kde.org/standards/kwix/0.1/"
    xmlns:wix="http://schemas.microsoft.com/wix/2006/wi"
>
<xsl:output method="xml" indent="yes"/>

<xsl:template match="kwix:KatePlugin">

  <wix:Component Directory="KTEPluginsDir">
    <wix:File KeyPath="yes">
      <xsl:attribute name="Source">lib\plugins\ktexteditor\<xsl:value-of select="@Name"/>.dll</xsl:attribute>
    </wix:File>
  </wix:Component>

  <xsl:if test="@KXmlGuiName">
    <wix:DirectoryRef Id="KXmlGuiDir">
      <wix:Directory>
        <xsl:attribute name="Id"><xsl:value-of select="@KXmlGuiName"/>_KXmlGuiDir</xsl:attribute>
        <xsl:attribute name="Name"><xsl:value-of select="@KXmlGuiName"/></xsl:attribute>
        <wix:Component>
          <wix:File KeyPath="yes">
            <xsl:attribute name="Id"><xsl:value-of select="@KXmlGuiName"/>_ui.rc</xsl:attribute>
            <xsl:attribute name="Source">share\kxmlgui5\<xsl:value-of select="@KXmlGuiName"/>\ui.rc</xsl:attribute>
          </wix:File>
        </wix:Component>
      </wix:Directory>
    </wix:DirectoryRef>
  </xsl:if>

  <wix:ComponentGroup>
    <xsl:attribute name="Id"><xsl:value-of select="@Name"/></xsl:attribute>
    <wix:ComponentRef>
      <xsl:attribute name="Id"><xsl:value-of select="@Name"/>.dll</xsl:attribute>
    </wix:ComponentRef>
    <xsl:if test="@KXmlGuiName">
      <wix:ComponentRef>
        <xsl:attribute name="Id"><xsl:value-of select="@KXmlGuiName"/>_ui.rc</xsl:attribute>
      </wix:ComponentRef>
    </xsl:if>
    <xsl:apply-templates select="*"/>
  </wix:ComponentGroup>

</xsl:template>

<xsl:template match="kwix:XmlToolsDTD">

  <wix:Component Directory="KateXmlToolsDir">
    <wix:File KeyPath="yes">
      <xsl:attribute name="Source">share\katexmltools\<xsl:value-of select="@Name"/></xsl:attribute>
    </wix:File>
  </wix:Component>

</xsl:template>

<xsl:template match="@*|wix:*">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

</xsl:stylesheet>
