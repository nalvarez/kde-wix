function(_WIX_COMPILE wixobj source objdir flags)
    get_filename_component(sourcename ${source} NAME)
    get_filename_component(source ${source} REALPATH)
    string(REGEX REPLACE "\\.wxs$" ".wixobj" objpath "${source}")
    string(REGEX REPLACE "^${CMAKE_SOURCE_DIR}" "" objpath "${objpath}")
    string(REGEX REPLACE "^${CMAKE_BINARY_DIR}" "" objpath "${objpath}")
    string(REGEX REPLACE "^/" "" objpath "${objpath}")
    set(OUTPUT_WIXOBJ "${objdir}/${objpath}")

    add_custom_command(
        OUTPUT ${OUTPUT_WIXOBJ}
        COMMAND "${WIX_CANDLE_PATH}" -nologo ${flags} -o "${OUTPUT_WIXOBJ}" ${source}
        MAIN_DEPENDENCY ${source}
        COMMENT "Compiling ${sourcename}"
    )
    set(${wixobj} ${OUTPUT_WIXOBJ} PARENT_SCOPE)
endfunction()

function(_WIX_LINK target_name extension)
    CMAKE_PARSE_ARGUMENTS(_WIX_AP "" "" "SOURCES;COMPILE_FLAGS;LINK_FLAGS;DEPENDS" ${ARGN})

    set(objs "")
    foreach(source ${_WIX_AP_SOURCES})
        _WIX_COMPILE(obj ${source} "CMakeFiles/${target_name}.dir" "${_WIX_AP_COMPILE_FLAGS}")
        set(objs ${objs} ${obj})
    endforeach()

    set(output_name "${target_name}.${extension}")
    add_custom_command(
        OUTPUT ${output_name} ${target_name}.wixpdb
        COMMAND "${WIX_LIGHT_PATH}" -nologo
            -out "${output_name}"
            ${_WIX_AP_LINK_FLAGS}
            ${objs}
        DEPENDS ${objs} ${_WIX_AP_DEPENDS}
        COMMENT "Linking ${output_name}"
    )
    add_custom_target(${target_name} ALL DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${output_name})
endfunction()

function(WIX_ADD_PRODUCT target_name)
    _WIX_LINK(${target_name} msi ${ARGN})
endfunction()
function(WIX_ADD_BUNDLE target_name)
    _WIX_LINK(${target_name} exe ${ARGN})
endfunction()

function(WIX_HARVEST)
    CMAKE_PARSE_ARGUMENTS(_HV "AUTOGEN_GUIDS" "OUTPUT;DIRECTORY;INDENT;COMPONENT_GROUP;ROOT_DIRECTORY;VARIABLE;COMMENT" "" ${ARGN})
    if(NOT _HV_OUTPUT OR NOT _HV_DIRECTORY)
        message(FATAL_ERROR "OUTPUT and DIRECTORY are required")
    endif()
    set(cmdline "-nologo")
    if(_HV_AUTOGEN_GUIDS)
        set(cmdline "${cmdline};-ag")
    endif()
    if(_HV_INDENT)
        set(cmdline "${cmdline};-indent;${_HV_INDENT}")
    endif()
    if(_HV_COMPONENT_GROUP)
        set(cmdline "${cmdline};-cg;${_HV_COMPONENT_GROUP}")
    endif()
    if(_HV_ROOT_DIRECTORY)
        set(cmdline "${cmdline};-dr;${_HV_ROOT_DIRECTORY}")
    endif()
    if(_HV_VARIABLE)
        set(cmdline "${cmdline};-var;${_HV_VARIABLE}")
    endif()
    if(NOT _HV_COMMENT)
        set(_HV_COMMENT "Harvesting ${_HV_DIRECTORY}")
    endif()
    add_custom_command(
        OUTPUT ${_HV_OUTPUT}
        COMMAND ${WIX_HEAT_PATH} dir ${_HV_DIRECTORY} -o ${_HV_OUTPUT} ${cmdline}
        VERBATIM
        COMMENT "${_HV_COMMENT}"
    )
endfunction()
