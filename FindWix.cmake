# - Find WiX executables
#=============================================================================
# Copyright 2013 Nicolás Alvarez
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

set(_WIX_VERSIONS 3.9 3.8 3.7 3.6 3.5 3.0)

foreach(_VERSION ${_WIX_VERSIONS})
    # CMake is a 32-bit process, so on 64-bit systems, its access to the
    # registry would be normally redirected to Wow6432Node. But CMake has
    # explicit logic to look the same registry area the compiled app would see;
    # in other words, it will access the 64-bit registry if we're compiling
    # for 64-bit (according to CMAKE_SIZEOF_VOID_P).
    # However, WiX is always 32-bit, and will always be in the 32-bit registry
    # This is why we need to manually look in both registry keys.
    list(APPEND _REG_SEARCH_PATHS
        "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows Installer XML\\${_VERSION};InstallRoot]"
        "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Microsoft\\Windows Installer XML\\${_VERSION};InstallRoot]"
    )
endforeach()

find_path(
    WIX_PATH
    candle.exe
    PATHS ${_REG_SEARCH_PATHS}
)

find_program(WIX_CANDLE_PATH candle.exe PATHS ${WIX_PATH} NO_DEFAULT_PATH)
find_program(WIX_LIGHT_PATH  light.exe  PATHS ${WIX_PATH} NO_DEFAULT_PATH)
find_program(WIX_LIT_PATH    lit.exe    PATHS ${WIX_PATH} NO_DEFAULT_PATH)
find_program(WIX_HEAT_PATH   heat.exe   PATHS ${WIX_PATH} NO_DEFAULT_PATH)

# Extract the WiX version number by looking at candle's help output.

execute_process(
    COMMAND "${WIX_CANDLE_PATH}" "-help"
    OUTPUT_VARIABLE _CANDLE_HELP_OUTPUT
)
string(REGEX REPLACE "^([^\n]*)\n.*" "\\1" _CANDLE_HELP_OUTPUT "${_CANDLE_HELP_OUTPUT}")

# format of WiX 3.5
if(_CANDLE_HELP_OUTPUT MATCHES "^Microsoft \\(R\\) Windows Installer Xml Compiler version ([0-9]+(\\.[0-9]+)*)$")
    set(WIX_VERSION "${CMAKE_MATCH_1}")
# format of WiX 3.6 and 3.7
elseif(_CANDLE_HELP_OUTPUT MATCHES "^Windows Installer Xml Compiler version ([0-9]+(\\.[0-9]+)*)$")
    set(WIX_VERSION "${CMAKE_MATCH_1}")
# format of WiX 3.8
elseif(_CANDLE_HELP_OUTPUT MATCHES "^Windows Installer XML Toolset Compiler version ([0-9]+(\\.[0-9]+)*)")
    set(WIX_VERSION "${CMAKE_MATCH_1}")
endif()

if(MSVC)
    if(CMAKE_CXX_COMPILER_VERSION MATCHES "^16\\.")
        set(_MSVC_VERSION "2010")
    elseif(CMAKE_CXX_COMPILER_VERSION MATCHES "^17\\.")
        set(_MSVC_VERSION "2012")
    elseif(CMAKE_CXX_COMPILER_VERSION MATCHES "^18\\.")
        set(_MSVC_VERSION "2013")
    else()
        message(WARNING "Unknown MSVC version")
    endif()

    if(_MSVC_VERSION)
        find_path(WIX_INCLUDE_DIR wcautil.h PATHS ${WIX_PATH}/../SDK/VS${_MSVC_VERSION}/inc)
        # TODO find libraries too
    endif()
endif()

include(WixMacros.cmake)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Wix
    REQUIRED_VARS WIX_PATH WIX_CANDLE_PATH WIX_LIGHT_PATH WIX_LIT_PATH
    VERSION_VAR WIX_VERSION)

